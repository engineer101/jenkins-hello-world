## Abstract
This repo containt a simple java springboot applicaitons with junit tests

## Implement a code snippet of Jenkins Pipeline as code with the below stages

### 1. Stage A --> Checkout a sample code ( Eg :- Simple project to support below scenarios)
Depending on your preferences setuping of that stage can be done in a number of different ways. Current setup uses gitlab plugin

### 2. Stage B --> Run the tests, and make sure 2-3 tests fails (Eg: Consider any basic tests ,delibaretly fail more than 2 tests)
Pipeline consists of two stages: unit tests and sonarqube static analysis

### 3.Stage C --> Collect the Summary of the tests and write to excel file which is hosted at an centralized place (where 2 or more pipelines can write the results into the excel file)
### (Eg:- Summary of tests means get the Failed Test Case Name , Failed Count and write to an centralized hosted excel file)

Assuming that "centralized place" would mean an NFS share with a file called `jenkins_reports.xlsx` with predefined headers like the following 

|JobName  | FailedStages count  | FailedStages names  | timestamp |
|---|---|---|--|
|   |   |   |  |

If a test fails its name gets added to a variable `failedStages` and during the final `post` stage python script will add a row to the xlsx file
