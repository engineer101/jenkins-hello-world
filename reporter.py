#!/usr/bin/python3


import datetime
import libnfs
import os
import openpyxl


nfs = libnfs.NFS('nfs://1.3.3.7/data/')
failed_stages = os.getenv('FAILEDSTAGES').translate(str.maketrans('','','[]')).split(",")
timestamp = datetime.datetime.now()
row = (os.getenv('JOB_NAME'), len(failed_stages), failed_stages, timestamp)


def add_row(filename, row):
    with nfs.open(filename) as f:
        book = openpyxl.load_workbook(f)
        book.active.append(row)
        book.save(f)


if __name__ == "__main__":
    add_row('jenkins_reports.xlsx', row)
