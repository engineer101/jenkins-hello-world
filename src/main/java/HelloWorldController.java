package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @RequestMapping("/")
    public String helloWorld() {
        return "{'message': 'Hello World'}";
    }

    @RequestMapping("/version")
    public String version() {
        return "{'version': '0.0.1'}";
    }

}
